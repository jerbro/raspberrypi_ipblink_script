

import time
import socket

def get_interface_ip():
	s = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
	try:
		s.connect(('10.255.255.255',0))
		IP = s.getsockname()[0]
	except:
		IP='127.0.0.1'
	finally:
		s.close()
	return IP


def writeValue(val,duration):
	f=open('/sys/class/leds/led0/brightness','w')
	f.write(val)
	f.close()
	time.sleep(duration)
	return 0



def blinkLed(ontime, offtime):
	writeValue('1',ontime)
	writeValue('0',offtime)
	return 0

def blinkDigit(digit):
	if digit ==0:
		digit = 10
	for i in range(1,digit+1):
		print "blink nr: "+str(i)
		blinkLed(0.1,0.3)

def blinkNumber(number):
	text = str(number)
	for ch in text:
		blinkDigit(int(ch))
		time.sleep(1.5)
		


for i in range(0,15):
	blinkLed(0.1,0.1)
blinkLed(0,2)


IP = get_interface_ip()
IPnrs = IP.split('.')
for ipnr in IPnrs:
	blinkNumber(ipnr)
	blinkLed(2,2)

print IPnrs


blinkDigit(1)

print IP